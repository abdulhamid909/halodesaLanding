<!DOCTYPE html>
<html>

<head>
    @include('partial.head')
    @yield('style')

</head>

<body class="flat-blue landing-page">
    <nav class="navbar navbar-inverse navbar-fixed-top  navbar-affix {!! Request::segment(1) == "" ? '' : 'background-navbar' !!}" role="navigation" data-spy="affix" data-offset-top="60">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Menu</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{!! url('/') !!}">
                    <div class="icon fa fa-home"></div>
                    <div class="title">Halo Desa</div>
                </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse " aria-expanded="true">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="{!! url('/') !!}">Home</a></li>
                    <li><a href="{!! url('fitur') !!}">Fitur</a></li>
                    <li><a href="{!! url('kontak') !!}">Kontak</a></li>
                    <li><a href="#contact">Call : 085640108325</a></li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </nav>
    <div class="content-body">
        @yield('content')
        <div class="container-fluid app-content-b contact-us">
            <div class="container">
                <div class="row featurette">
                    <div class="col-md-6"><h2 class="color-white contact-us-header">Hubungi Kami</h2>
                    <p class="color-white contact-us-description">Hubungi Kami jika anda merasa memiliki ketertarikan. <br/>
                        "Teknologi Semakin Kedepan semakin diterapkan di semua lini kehidupan. Jika kita mau mengikuti kita 
                        akan terus berkembang dan merasakan manfaatnya tapi jika kita tidak mengikuti Teknologi kita akan teringgal dan tidak bisa menikmati mudah-nya hidup dengan Teknologi"
                    </p></div>
                    <div class="col-md-6">
                        <form action="{!! url('send') !!}" method="POST">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-sm-6"><input id="name" required="true" name="name" type="text" class="form-control" placeholder="Full Name"> </div>
                                <div class="col-sm-6"><input id="email" required="true" name="email" type="email" class="form-control" placeholder="Email address"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12"><textarea id="message" required="true" name="message" class="form-control" placeholder="Your Message" rows="5"></textarea></div>
                            </div>
                            <div>
                                <button id="contact-submit" type="submit" class="btn btn-success pull-right">Send</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @include('components.info')
    </div>
    <!-- /END THE FEATURETTES -->
    <!-- FOOTER -->
    <footer class="app-footer">
      <div class="container">
        <p class="text-muted">&copy; {!! date('Y') !!}, Halo Desa Team.</p>
      </div>
    </footer>
    <!-- Javascript Libs -->
    <script type="text/javascript" src="{!! url('') !!}/dist/js/jquery.min.js"></script>
    <script type="text/javascript" src="{!! url('') !!}/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{!! url('') !!}/dist/js/Chart.min.js"></script>
    <script type="text/javascript" src="{!! url('') !!}/dist/js/bootstrap-switch.min.js"></script>

    <script type="text/javascript" src="{!! url('') !!}/dist/js/jquery.matchHeight-min.js"></script>
    <script type="text/javascript" src="{!! url('') !!}/dist/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{!! url('') !!}/dist/js/dataTables.bootstrap.min.js"></script>

    <script type="text/javascript" src="{!! url('') !!}/dist/js/select2.full.min.js"></script>
    <script type="text/javascript" src="{!! url('') !!}/dist/js/ace/ace.js"></script>
    <script type="text/javascript" src="{!! url('') !!}/dist/js/ace/mode-html.js"></script>
    <script type="text/javascript" src="{!! url('') !!}/dist/js/ace/theme-github.js"></script>
    <!-- Javascript -->
    <script type="text/javascript" src="{!! url('') !!}/js/app.js"></script>
    <!-- <script type="text/javascript" src="{!! url('') !!}/js/index.js"></script> -->
    @yield('script')
    <!-- /.container -->
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (
            function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/57049a7022b83c4b6a61cf82/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->
</body>

</html>
