<!DOCTYPE html>
<html>

<head>
    @include('partial.head')
    @yield('style')
</head>

<body class="flat-blue">
    <div class="app-container">
        <div class="row content-container">
            @include('partial.header')

            @include('partial.sidebar')
            <!-- Main Content -->
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
        <footer class="app-footer">
            <div class="wrapper">
                <span class="pull-right">2.1 <a href="#"><i class="fa fa-long-arrow-up"></i></a></span> © {!! date('Y') !!} Copyright.
            </div>
        </footer>
        <div>
            <!-- Javascript Libs -->
            <script type="text/javascript" src="{!! url('') !!}/dist/js/jquery.min.js"></script>
            <script type="text/javascript" src="{!! url('') !!}/dist/js/bootstrap.min.js"></script>
            <script type="text/javascript" src="{!! url('') !!}/dist/js/Chart.min.js"></script>
            <script type="text/javascript" src="{!! url('') !!}/dist/js/bootstrap-switch.min.js"></script>

            <script type="text/javascript" src="{!! url('') !!}/dist/js/jquery.matchHeight-min.js"></script>
            <script type="text/javascript" src="{!! url('') !!}/dist/js/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="{!! url('') !!}/dist/js/dataTables.bootstrap.min.js"></script>

            <script type="text/javascript" src="{!! url('') !!}/dist/js/select2.full.min.js"></script>
            <script type="text/javascript" src="{!! url('') !!}/dist/js/ace/ace.js"></script>
            <script type="text/javascript" src="{!! url('') !!}/dist/js/ace/mode-html.js"></script>
            <script type="text/javascript" src="{!! url('') !!}/dist/js/ace/theme-github.js"></script>
            <!-- Javascript -->
            <script type="text/javascript" src="{!! url('') !!}/js/app.js"></script>
            <!-- <script type="text/javascript" src="{!! url('') !!}/js/index.js"></script> -->
            @yield('script')
</body>

</html>
