<div class="side-menu sidebar-inverse">
    <nav class="navbar navbar-default" role="navigation">
        <div class="side-menu-container">
            <div class="navbar-header">
                <a class="navbar-brand" href="{!! url('home') !!}">
                    <div class="icon fa fa-paper-plane"></div>
                    <div class="title">Administrator</div>
                </a>
                <button type="button" class="navbar-expand-toggle pull-right visible-xs">
                    <i class="fa fa-times icon"></i>
                </button>
            </div>
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="{!! url('home') !!}">
                        <span class="icon fa fa-tachometer"></span><span class="title">Dashboard</span>
                    </a>
                </li>

                <li class="panel panel-default dropdown">
                    <a data-toggle="collapse" href="#dropdown-master">
                        <span class="icon fa fa-file-text-o"></span><span class="title">Master</span>
                    </a>
                    <!-- Dropdown level 1 -->
                    <div id="dropdown-master" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul class="nav navbar-nav">
                                <li><a href="{!! url('groups') !!}">Groups</a></li>
                                <li><a href="{!! url('users') !!}">Users</a></li>
                                <li><a href="{!! url('articles-category') !!}">Kategori Artikel</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <!-- Dropdown-->
                <li class="panel panel-default dropdown">
                    <a data-toggle="collapse" href="#component-example">
                        <span class="icon fa fa-cubes"></span><span class="title">Modul</span>
                    </a>
                    <!-- Dropdown level 1 -->
                    <div id="component-example" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul class="nav navbar-nav">
                                <li><a href="{!! url('/articles') !!}">Artikel</a></li>
                                <li><a href="{!! url('/product') !!}">Produk</a></li>
                                <li><a href="{!! url('/pricing') !!}">Pricing</a></li>
                            </ul>
                        </div>
                    </div>
                </li>

                <li>
                    <a href="{!! url('setting') !!}">
                        <span class="icon fa fa-thumbs-o-up"></span><span class="title">Setting</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
</div>