@extends('layouts.backend')

@section('style')

@stop

@section('content')
<div class="side-body">
    <div class="page-title">
        <span class="title">Form UI Kits</span>
        <div class="description">A ui elements use in form, input, select, etc.</div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <div class="title">Form Elements</div>
                    </div>
					<div class="pull-right card-action">
					    <div class="btn-group" role="group">
					        <a href="{!! url(GLobalHelper::indexUrl()) !!}" class="btn btn-default" data-toggle="modal" >Back</a>
					    </div>
					</div>
                </div>
                <div class="card-body">
					@if(Session::has('message'))
					{!! GlobalHelper::messages(Session::get('message')) !!}
					@endif

					{{-- Form --}}
					{!! form_start($form) !!}
                        <div class="col-md-8">
                            {!! form_row($form->title, ['default_value' => isset($row) ? $row->title: '']) !!}
                            {!! form_row($form->slug, ['default_value' => isset($row) ? $row->slug: '']) !!}
                          
                            {!! form_row($form->content, ['default_value' => isset($row) ? $row->content: '']) !!}
                            {!! form_row($form->category_id, ['default_value' => isset($row) ? $row->category_id : '']) !!}
                            
                            {!! form_row($form->tag, ['default_value' => isset($row) ? $row->tag: '']) !!}
                            {!! form_row($form->status,$options = ['attr' => ['class' => 'styled']]) !!}
                            <fieldset>
                                <legend class="text-bold">Meta Data</legend>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Meta Title</label>
                                        <div class="input-group" style="width: 568px;">
                                            {!! form_row($form->meta_title, ['default_value' => isset($row) ? $row->meta_title: '']) !!}
                                            <span class="input-group-addon bg-slate-700" style="display: none;"><i class="icon-help"></i></span>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Meta Keyword</label>
                                    <div class="input-group" style="width: 568px;">
                                        {!! form_row($form->meta_keyword, ['default_value' => isset($row) ? $row->meta_keyword: '']) !!}
                                        <span class="input-group-addon bg-slate-700" style="display: none;"><i class="icon-help"></i></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Meta Desription</label>
                                    <div class="input-group" style="width: 568px;">
                                        {!! form_row($form->meta_description, ['default_value' => isset($row) ? $row->meta_description: '']) !!}
                                        <span class="input-group-addon bg-slate-700" style="display: none;"><i class="icon-help"></i></span>
                                    </div>
                                </div>

                            </fieldset>

                        </div>
                        <div class="col-md-4">
                            {!! form_label($form->image) !!}
                            {!! form_widget($form->image, ['attr' => ['style' => 'display:none']]) !!}
                            <div class="text-danger">{!! $errors->first('image') !!}</div>

                            {!! form_row($form->upload) !!}
                            <br>                        
                            <p class="text-center">
                                <img id="preview_img" class="img img-thumbnail" style="margin-top: 25px; max-height:200px" src="{{ isset($row) && !empty($row->image) ? asset(GLobalHelper::checkImage('images/articles/thumb_'.$row->image)) : '' }}"  />
                            </p>    
                        </div>  

					<div class="clearfix"></div>
					@include('partial.form_button')
					{!! form_end($form) !!}

					{{-- End Form --}}
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('script')
	<script type="text/javascript">
        function readUrl(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview_img').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        function chooseFile()
        {
            $('#file').click();
        }
    </script>
@stop
