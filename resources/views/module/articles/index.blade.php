@extends('layouts.backend')

@section('style')
    <style type="text/css">
        #center{
            text-align: center;
        }
    </style>
@stop

@section('content')

<div class="side-body">
    <div class="page-title">
        <span class="title">{!! $title !!}</span>
        <div class="description">with jquery Datatable for display data with most usage functional. such as search, ajax loading, pagination, etc.</div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                    	<div class="title">{!! $title !!}</div>
                    </div>
					<div class="pull-right card-action">
					    <div class="btn-group" role="group">
					        <a href="{!! url(GLobalHelper::indexUrl().'/create') !!}" type="button" class="btn btn-success" data-toggle="modal" >Add</a>
					    </div>
					</div>
                </div>
                <div class="card-header" style="display: none;">
                    <div class="card-title">
                    	<div class="title">
                    	<select class="form-control">
                    		<option>Filter Here</option>
                    	</select>
                    	</div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="datatable table table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Kategori</th>
                                <th>Title</th>
                                <th>Konten</th>
                                <th>Status</th>
                                <th>Tampil Di Index</th>
                                <th>Diubah Oleh</th>
                                <th>Dibuat At</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>

                       	<tbody>

                       	</tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('script')

<script type="text/javascript">
	$(document).ready(function() {
		var filter = '';
		datatable(filter);
	});

	function datatable(filter){
		return oTable = $('.datatable').DataTable({
			"order": [[ 1, "asc" ]],
			"bPaginate": true,
			"bLengthChange": true,
			"bFilter": true,
			"bSort": true,
			"bInfo": true,
			"bAutoWidth": true,
			"processing": true,
			"bDestroy": true,
			"serverSide": true,
            "columns" : [
                {data: 'category_id', name: 'category_id'},
                {data: 'title', name: 'title'},
                {data: 'content', name: 'content'},
                {data: 'status', name: 'status'},
                {data: 'show', name: 'show'},
                {data: 'updated_by', name: 'updated_by'},
                {data: 'created_at', name: 'created_at', searchable : false},
                {data: 'action', name: 'action', searchable : false}
            ],
	        "ajax": {
	            "url": "{!! url(GLobalHelper::indexUrl().'/data') !!}",
			    error: function (xhr, error, thrown) {
			    	alert("Something's Wrongs");
			    },
	            data: function (d) {
	            }
	        },
			fnDrawCallback: function(){
				$('[data-toggle="tooltip"]').tooltip();
			}
		});
	}

</script>
@stop
