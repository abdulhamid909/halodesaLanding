@extends('layouts.backend')

@section('style')

@stop

@section('content')
<div class="side-body">
    <div class="page-title">
        <span class="title">Form UI Kits</span>
        <div class="description">A ui elements use in form, input, select, etc.</div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <div class="title">Form Elements</div>
                    </div>
					<div class="pull-right card-action">
					    <div class="btn-group" role="group">
					        <a href="{!! url(GLobalHelper::indexUrl()) !!}" class="btn btn-default" data-toggle="modal" >Back</a>
					    </div>
					</div>
                </div>
                <div class="card-body">
					@if(Session::has('message'))
					{!! GlobalHelper::messages(Session::get('message')) !!}
					@endif

					{{-- Form --}}
					{!! form_start($form) !!}

					{!! form_rest($form) !!}

					<div class="clearfix"></div>
					@include('partial.form_button')
					{!! form_end($form) !!}

					{{-- End Form --}}
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('script')
	<script type="text/javascript">

    </script>
@stop
