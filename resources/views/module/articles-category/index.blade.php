@extends('layouts.backend')

@section('style')
    <style type="text/css">
        #center{
            text-align: center;
        }
    </style>
@stop

@section('content')

<div class="side-body">
    <div class="page-title">
        <span class="title">{!! $title !!}</span>
        <div class="description">with jquery Datatable for display data with most usage functional. such as search, ajax loading, pagination, etc.</div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                    	<div class="title">{!! $title !!}</div>
                    </div>
					<div class="pull-right card-action">
					    <div class="btn-group" role="group">
					        <a href="{!! url(GLobalHelper::indexUrl().'/create') !!}" type="button" class="btn btn-success" data-toggle="modal" >Add</a>
					    </div>
					</div>
                </div>
                <div class="card-header" style="display: none;">
                    <div class="card-title">
                    	<div class="title">
                    	<select class="form-control">
                    		<option>Filter Here</option>
                    	</select>
                    	</div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="datatable table table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Kategori</th>
                                <th>Deskripsi</th>
                                <th>Create By</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                       	<tbody>

                       	</tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('script')

<script type="text/javascript">
	$(document).ready(function() {
		var filter = '';
		datatable(filter);
	});

	function datatable(filter){
		return oTable = $('.datatable').DataTable({
			"order": [[ 1, "asc" ]],
			"bPaginate": true,
			"bLengthChange": true,
			"bFilter": true,
			"bSort": true,
			"bInfo": true,
			"bAutoWidth": true,
			"processing": true,
			"bDestroy": true,
			"serverSide": true,
	        "ajax": {
	            "url": "{!! url(GLobalHelper::indexUrl().'/data') !!}",
			    error: function (xhr, error, thrown) {
			    	alert("Something's Wrongs");
			    },
	            data: function (d) {
	            }
	        },
			fnDrawCallback: function(){
				$('[data-toggle="tooltip"]').tooltip();
			}
		});
	}

</script>
@stop
