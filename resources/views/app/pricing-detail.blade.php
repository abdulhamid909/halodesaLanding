@extends('layouts.app')

@section('title')
    {!! $title !!}
@stop 

@section('style')
    <style type="text/css">
        .sub-content{
            padding-top: 17px;   
        }
    </style>
@stop

@section('content')

<div class="sub-content">
    <div class="container-fluid app-content-a">
        <div class="">        
            <div class="row text-center">
                <div class="col-md-12">
                    <h1 class="headingOne">Paket {!! ucfirst($type) !!}</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid app-content-a" style="padding-top: 0px;">
        <div class="">        
        <div class="row text-center">

            <div class="col-md-12">
                <div class="form-group">
                    <label for="name" class="control-label" style="text-align: left;">Name</label>
                    <input class="form-control" name="name" type="text" value="" id="name">
                </div>
            </div>
            <div class="col-md-12 ">
            <div class="box-footer">
                <div class="pull-right">
                    <button class="btn btn-danger" type="button" id="reset">
                        <i class="fa fa-refresh"></i> Back
                    </button>
                    <button class="btn btn-primary" type="submit">
                        <i class="fa fa-file"></i> Save
                    </button>
                </div>
                <div class="clearfix"></div>
            </div>
            </div>

        </div>
        </div>
    </div>
</div>
@endsection
