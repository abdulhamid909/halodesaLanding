@extends('layouts.app')

@section('title')
    {!! $title !!}
@stop 

@section('style')
    <style type="text/css">
        .sub-content{
            padding-top: 17px;   
        }
    </style>
@stop

@section('content')

<div class="sub-content">
    <div class="container-fluid app-content-a">
        <div class="container">        
        <div class="row text-center">

            <div class="col-md-12">
                <div class="maps">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3952.6839094259467!2d110.38116811483161!3d-7.82324187986663!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a57a052ffdae3%3A0x4b9989538c6097e1!2sTLab!5e0!3m2!1sen!2s!4v1451889384762" width="100%" height="235px;" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>

        </div>
        </div>
    </div>
</div>
@endsection
