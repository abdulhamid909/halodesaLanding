<?php $__env->startSection('style'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="side-body">
    <div class="page-title">
        <span class="title">Form UI Kits</span>
        <div class="description">A ui elements use in form, input, select, etc.</div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <div class="title">Form Elements</div>
                    </div>
					<div class="pull-right card-action">
					    <div class="btn-group" role="group">
					        <a href="<?php echo url(GLobalHelper::indexUrl()); ?>" class="btn btn-default" data-toggle="modal" >Back</a>
					    </div>
					</div>
                </div>
                <div class="card-body">
					<?php if(Session::has('message')): ?>
					<?php echo GlobalHelper::messages(Session::get('message')); ?>

					<?php endif; ?>

					<?php /* Form */ ?>
					<?php echo form_start($form); ?>


					<?php echo form_rest($form); ?>


					<div class="clearfix"></div>
					<?php echo $__env->make('partial.form_button', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
					<?php echo form_end($form); ?>


					<?php /* End Form */ ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
	<script type="text/javascript">

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.backend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>