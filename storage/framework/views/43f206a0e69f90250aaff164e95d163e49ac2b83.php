<?php $__env->startSection('style'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="side-body">
    <div class="page-title">
        <span class="title">Form UI Kits</span>
        <div class="description">A ui elements use in form, input, select, etc.</div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <div class="title">Form Elements</div>
                    </div>
					<div class="pull-right card-action">
					    <div class="btn-group" role="group">
					        <a href="<?php echo url(GLobalHelper::indexUrl()); ?>" class="btn btn-default" data-toggle="modal" >Back</a>
					    </div>
					</div>
                </div>
                <div class="card-body">
					<?php if(Session::has('message')): ?>
					<?php echo GlobalHelper::messages(Session::get('message')); ?>

					<?php endif; ?>

					<?php /* Form */ ?>
					<?php echo form_start($form); ?>

                        <div class="col-md-8">
                            <?php echo form_row($form->title, ['default_value' => isset($row) ? $row->title: '']); ?>

                            <?php echo form_row($form->slug, ['default_value' => isset($row) ? $row->slug: '']); ?>

                          
                            <?php echo form_row($form->content, ['default_value' => isset($row) ? $row->content: '']); ?>

                            <?php echo form_row($form->category_id, ['default_value' => isset($row) ? $row->category_id : '']); ?>

                            
                            <?php echo form_row($form->tag, ['default_value' => isset($row) ? $row->tag: '']); ?>

                            <?php echo form_row($form->status,$options = ['attr' => ['class' => 'styled']]); ?>

                            <fieldset>
                                <legend class="text-bold">Meta Data</legend>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Meta Title</label>
                                        <div class="input-group" style="width: 568px;">
                                            <?php echo form_row($form->meta_title, ['default_value' => isset($row) ? $row->meta_title: '']); ?>

                                            <span class="input-group-addon bg-slate-700" style="display: none;"><i class="icon-help"></i></span>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Meta Keyword</label>
                                    <div class="input-group" style="width: 568px;">
                                        <?php echo form_row($form->meta_keyword, ['default_value' => isset($row) ? $row->meta_keyword: '']); ?>

                                        <span class="input-group-addon bg-slate-700" style="display: none;"><i class="icon-help"></i></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Meta Desription</label>
                                    <div class="input-group" style="width: 568px;">
                                        <?php echo form_row($form->meta_description, ['default_value' => isset($row) ? $row->meta_description: '']); ?>

                                        <span class="input-group-addon bg-slate-700" style="display: none;"><i class="icon-help"></i></span>
                                    </div>
                                </div>

                            </fieldset>

                        </div>
                        <div class="col-md-4">
                            <?php echo form_label($form->image); ?>

                            <?php echo form_widget($form->image, ['attr' => ['style' => 'display:none']]); ?>

                            <div class="text-danger"><?php echo $errors->first('image'); ?></div>

                            <?php echo form_row($form->upload); ?>

                            <br>                        
                            <p class="text-center">
                                <img id="preview_img" class="img img-thumbnail" style="margin-top: 25px; max-height:200px" src="<?php echo e(isset($row) && !empty($row->image) ? asset(GLobalHelper::checkImage('images/articles/thumb_'.$row->image)) : ''); ?>"  />
                            </p>    
                        </div>  

					<div class="clearfix"></div>
					<?php echo $__env->make('partial.form_button', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
					<?php echo form_end($form); ?>


					<?php /* End Form */ ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
	<script type="text/javascript">
        function readUrl(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview_img').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        function chooseFile()
        {
            $('#file').click();
        }
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.backend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>