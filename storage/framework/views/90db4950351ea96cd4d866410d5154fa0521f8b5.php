<section class="for-info">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6">
				<div class="short-info wow flipInX">
					<div class="icon"><i class="zmdi zmdi-phone"></i></div>
					<h1>+62856 4010 8325</h1>
					<p>
						jam operasi kami <br>
						Senin - Minggu 24 Jam
					</p>
				</div>
			</div>
			<div class="col-md-6 col-sm-6">
				 <div class="short-info wow flipInX">
					<div class="icon"><i class="zmdi zmdi-email"></i></div>
					<h1>infohalodesa@gmail.com</h1>
					<p>Silahkan Hubungi Kami secepatnya, kami akan respon anda secepatnya juga.
					</p>
				</div>
			</div>
		</div>
	</div>
</section>