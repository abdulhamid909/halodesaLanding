<!DOCTYPE html>
<html>

<head>
    <?php echo $__env->make('partial.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->yieldContent('style'); ?>
</head>

<body class="flat-blue">
    <div class="app-container">
        <div class="row content-container">
            <?php echo $__env->make('partial.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php echo $__env->make('partial.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <!-- Main Content -->
            <div class="container-fluid">
                <?php echo $__env->yieldContent('content'); ?>
            </div>
        </div>
        <footer class="app-footer">
            <div class="wrapper">
                <span class="pull-right">2.1 <a href="#"><i class="fa fa-long-arrow-up"></i></a></span> © <?php echo date('Y'); ?> Copyright.
            </div>
        </footer>
        <div>
            <!-- Javascript Libs -->
            <script type="text/javascript" src="<?php echo url(''); ?>/dist/js/jquery.min.js"></script>
            <script type="text/javascript" src="<?php echo url(''); ?>/dist/js/bootstrap.min.js"></script>
            <script type="text/javascript" src="<?php echo url(''); ?>/dist/js/Chart.min.js"></script>
            <script type="text/javascript" src="<?php echo url(''); ?>/dist/js/bootstrap-switch.min.js"></script>

            <script type="text/javascript" src="<?php echo url(''); ?>/dist/js/jquery.matchHeight-min.js"></script>
            <script type="text/javascript" src="<?php echo url(''); ?>/dist/js/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="<?php echo url(''); ?>/dist/js/dataTables.bootstrap.min.js"></script>

            <script type="text/javascript" src="<?php echo url(''); ?>/dist/js/select2.full.min.js"></script>
            <script type="text/javascript" src="<?php echo url(''); ?>/dist/js/ace/ace.js"></script>
            <script type="text/javascript" src="<?php echo url(''); ?>/dist/js/ace/mode-html.js"></script>
            <script type="text/javascript" src="<?php echo url(''); ?>/dist/js/ace/theme-github.js"></script>
            <!-- Javascript -->
            <script type="text/javascript" src="<?php echo url(''); ?>/js/app.js"></script>
            <!-- <script type="text/javascript" src="<?php echo url(''); ?>/js/index.js"></script> -->
            <?php echo $__env->yieldContent('script'); ?>
</body>

</html>
