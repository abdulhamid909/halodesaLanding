<?php $__env->startSection('title'); ?>
    <?php echo $title; ?>

<?php $__env->stopSection(); ?> 

<?php $__env->startSection('content'); ?>
<div class="sub-content">
    <div class="jumbotron app-header">
        <div class="container">
            <h2 class="text-center"><i class="app-logo fa fa-connectdevelop fa-5x color-white"></i><div class="color-white">Halo Desa</div></h2>
            <p class="text-center color-white app-description">Halo Desa merupakan Sistem Informasi Manajemen Desa, baik yang mencakup mengenai Informasi Desa, Pelaporan / Pengaduan, Pengarsipan serta Manajemen Surat Menyurat. Sistem Ini tidak hanya bisa digunakan di Pedesaan atau Kelurahan melainkan juga bisa diterapkan ke tingkat diatasnya baik itu Kecamatan Maupun Kabupaten</p>
            <p class="text-center"><a class="btn btn-primary btn-lg app-btn" href="#" role="button">Selengkapnya »</a></p>
        </div>
    </div>
    <div class="container-fluid app-content-a">
        <div class="container">        <div class="row text-center">
            <div class="col-md-4 col-sm-6">
                <span class="fa-stack fa-lg fa-5x">
                  <i class="fa fa-circle-thin fa-stack-2x"></i>
                  <i class="fa fa-info fa-stack-1x"></i>
                </span>
                <h2>Sistem Informasi & Komunikasi Desa</h2>
                <p>Memberikan Informasi Kepada Masyarakat Desa secara cepat dan Realtime.</p>
            </div>
            <!-- /.col-lg-4 -->
            <div class="col-md-4 col-sm-6">
                <span class="fa-stack fa-lg fa-5x">
                  <i class="fa fa-circle-thin fa-stack-2x"></i>
                  <i class="fa fa-inbox fa-stack-1x"></i>
                </span>
                <h2>Pengaduan & Pelaporan Masyarakat Desa</h2>
                <p>Masyarakat Bisa Melakukan Pengaduan Kapanpun dan Diamanpun Bisa dengan SMS, ataupun dengan membuka Web Halo Desa.</p>
            </div>
            <!-- /.col-lg-4 -->
            <div class="col-md-4 col-sm-6">
                <span class="fa-stack fa-lg fa-5x">
                  <i class="fa fa-circle-thin fa-stack-2x"></i>
                  <i class="fa fa-book fa-stack-1x"></i>
                </span>
                <h2>Pengarsipan & Manajemen Surat</h2>
                <p>Tidak sampai disitu saja pihak kelurahan akan dapat dengan mudah melakukan Pengarsipan dan Manajemen Surat Menyurat.</p>
            </div>
            <!-- /.col-lg-4 -->
        </div>
        </div>
    </div>
    <div class="container-fluid app-content-b feature-1">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-6">
                </div>
                <div class="col-md-5 col-sm-6 text-right color-white">
                    <h2 class="featurette-heading"> "Hidup itu arahnya kedepan".</h2>
                    <p class="lead">" Dengan Teknologi kita ikut terlibat dalam mengubah status Bangsa Kita dari Bangsa Berkembang menjadi Bangsa Maju".</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid app-content-a">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center app-content-header">Biaya Berapa ?</h2>
                    <p class="text-center app-content-description">Biaya yang harus dikeluarkan tergantung dengan paket yang kita pilih. Kita fleksibel dengan fitur - fitur yang anda inginkan.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row no-margin no-gap">
                        <div class="col-md-3 col-sm-6">
                            <div class="pricing-table dark-blue">
                                <div class="pt-header">
                                    <div class="plan-pricing">
                                        <div class="pricing">3.500.000</div>
                                        <div class="pricing-type">Sekali Bayar</div>
                                    </div>
                                </div>
                                <div class="pt-body">
                                    <h4>Basic Plan</h4>
                                    <ul class="plan-detail">
                                        <li>Web Informasi Desa</li>
                                        <li>Web Admin Manajemen Web Desa</li>
                                        <li>Gratis Support Selamanya</li>
                                    </ul>
                                </div>
                                <div class="pt-footer">
                                    <a target="_blank" href="<?php echo url('/pricing-detail/basic'); ?>" class="btn btn-primary">Beli Sekarang</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="pricing-table green">
                                <div class="pt-header">
                                    <div class="plan-pricing">
                                        <div class="pricing">6.500.000</div>
                                        <div class="pricing-type">Sekali Bayar</div>
                                    </div>
                                </div>
                                <div class="pt-body">
                                    <h4>Standard Plan</h4>
                                    <ul class="plan-detail">
                                        <li>Web dan Admin Web Desa</li>
                                        <li>Pengaduan Desa Melalui Website</li>
                                        <li>Gratis Support Selamanya</li>
                                    </ul>
                                </div>
                                <div class="pt-footer">
                                    <a target="_blank" href="<?php echo url('/pricing-detail/standart'); ?>" class="btn btn-primary">Beli Sekarang</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="pricing-table  dark-blue">
                                <div class="pt-header">
                                    <div class="plan-pricing">
                                        <div class="pricing">Call</div>
                                        <div class="pricing-type">Call Marketing</div>
                                    </div>
                                </div>
                                <div class="pt-body">
                                    <h4>Advanced Plan</h4>
                                    <ul class="plan-detail">
                                        <li>Web dan Admin Web Desa</li>
                                        <li>Pengaduan Web & SMS serta SMS Broadcast</li>
                                        <li>Gratis Support Selamanya</li>
                                    </ul>
                                </div>
                                <div class="pt-footer">
                                    <a target="_blank" href="<?php echo url('/pricing-detail/advanced'); ?>" class="btn btn-primary">Beli Sekarang</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="pricing-table dark-blue">
                                <div class="pt-header">
                                    <div class="plan-pricing">
                                        <div class="pricing">Call</div>
                                        <div class="pricing-type">Call Marketing</div>
                                    </div>
                                </div>
                                <div class="pt-body">
                                    <h4>Unlimited Plan</h4>
                                    <ul class="plan-detail">
                                        <li>Fitur Disesuaikan Kebutuhan Kantor</li>
                                        <li>-</li>
                                        <li>-</li>
                                    </ul>
                                </div>
                                <div class="pt-footer">
                                    <a target="_blank" href="<?php echo url('/pricing-detail/unlimited'); ?>" class="btn btn-primary">Beli Sekarang</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>