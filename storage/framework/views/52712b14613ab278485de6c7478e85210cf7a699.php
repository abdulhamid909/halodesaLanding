<?php $__env->startSection('style'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="side-body">
    <div class="page-title">
        <span class="title"><?php echo $title; ?></span>
        <div class="description">with jquery Datatable for display data with most usage functional. such as search, ajax loading, pagination, etc.</div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                    	<div class="title"><?php echo $title; ?></div>
                    </div>
					<div class="pull-right card-action">
					    <div class="btn-group" role="group">
					        <a href="<?php echo url(GLobalHelper::indexUrl().'/create'); ?>" type="button" class="btn btn-success" data-toggle="modal" >Add</a>
					    </div>
					</div>
                </div>
                <div class="card-header" style="display: none;">
                    <div class="card-title">
                    	<div class="title">
                    	<select class="form-control">
                    		<option>Filter Here</option>
                    	</select>
                    	</div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="datatable table table-striped" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Create By</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                       	<tbody>

                       	</tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>

<script type="text/javascript">
	$(document).ready(function() {
		var filter = '';
		datatable(filter);
	});

	function datatable(filter){
		return oTable = $('.datatable').DataTable({
			"order": [[ 1, "asc" ]],
			"bPaginate": true,
			"bLengthChange": true,
			"bFilter": true,
			"bSort": true,
			"bInfo": true,
			"bAutoWidth": true,
			"processing": true,
			"bDestroy": true,
			"serverSide": true,
	        "ajax": {
	            "url": "<?php echo url(GLobalHelper::indexUrl().'/data'); ?>",
			    error: function (xhr, error, thrown) {
			    	alert("Something's Wrongs");
			    },
	            data: function (d) {
	            }
	        },
			fnDrawCallback: function(){
				$('[data-toggle="tooltip"]').tooltip();
			}
		});
	}

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.backend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>