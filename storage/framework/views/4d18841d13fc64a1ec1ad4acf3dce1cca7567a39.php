<?php $__env->startSection('title'); ?>
    <?php echo $title; ?>

<?php $__env->stopSection(); ?> 

<?php $__env->startSection('style'); ?>
    <style type="text/css">
        .sub-content{
            padding-top: 17px;   
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="sub-content">
    <div class="container-fluid app-content-a">
        <div class="">        
            <div class="row text-center">
                <div class="col-md-12">
                    <h1 class="headingOne">Paket <?php echo ucfirst($type); ?></h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid app-content-a" style="padding-top: 0px;">
        <div class="">        
        <div class="row text-center">

            <div class="col-md-12">
                <div class="form-group">
                    <label for="name" class="control-label" style="text-align: left;">Name</label>
                    <input class="form-control" name="name" type="text" value="" id="name">
                </div>
            </div>
            <div class="col-md-12 text-center">
            <div class="box-footer">
                <div class="pull-right">
                    <button class="btn btn-danger" type="button" id="reset">
                        <i class="fa fa-refresh"></i> Back
                    </button>
                    <button class="btn btn-primary" type="submit">
                        <i class="fa fa-file"></i> Save
                    </button>
                </div>
                <div class="clearfix"></div>
            </div>
            </div>

        </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>