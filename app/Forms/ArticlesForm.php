<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class ArticlesForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('title','text')
            ->add('category_id', 'select',
                [
                    'attr'        => ['class' => 'form-control'],
                    'choices' => \App\Models\ArticlesCategory::lists("category_name", "id")->toArray(),
                    'label' => 'Category'
                ]
            )
            ->add('slug','text',[
                    'attr' => ['class' => 'form-control'],
                    'label' => 'Slug'
                ])
            ->add('content', 'textarea',
                [
                    'attr' => ['class' => 'form-control wysihtml5 wysihtml5-min','id'=>"summernote"],
                    'label' => 'Description'
                ]
            )
            ->add('tag','text',[
                    'attr' => ['class' => 'form-control tokenfield-teal']
                ])

            ->add('status', 'choice', [
                'choices' => ['1' => 'Active', '0' => 'Not Active'],
                'label'    => "Status",
                'expanded' => true,
                'multiple' => false
            ])
            
            /* 
            ** Meta Data
            */

            ->add('meta_title','text',[
                    'label' => false
                ])
            ->add('meta_keyword','text',[
                    'label' => false
                ])
            ->add('meta_description','text',[
                    'label' => false
                ])

            ->add('image','file',[
                'attr' => [
                    'id' => 'file',
                    'onchange' => 'readUrl(this)'
                ]
            ])
            ->add('upload','button',[
                'label' => '<i class="fa fa-upload"></i> Browse',
                'attr' => [
                    'class' => 'form-control btn btn-primary',
                    'onclick' => 'chooseFile()'
                ] 
            ]);
    }
}
