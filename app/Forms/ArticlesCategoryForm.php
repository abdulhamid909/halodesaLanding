<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class ArticlesCategoryForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('category_name', 'text',
                [
                    'attr' => ['class' => 'form-control']
                ]
            )
            ->add('slug', 'text',
                [
                    'attr' => ['class' => 'form-control']
                ]
            )
            ->add('description', 'textarea',
                [
                    'attr' => ['class' => 'wysihtml52 form-control']
                ]
            );
    }
}
