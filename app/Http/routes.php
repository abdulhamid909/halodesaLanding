<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/home', 'HomeController@index');
    Route::controller('/groups', 'DefaultMaster\GroupsController');
    Route::controller('/articles-category', 'ArticlesCategoryController');
    Route::controller('/articles', 'ArticlesController');
});


Route::group(['middleware' => ['web']], function () {
	
    Route::controller('/', 'AppController');

});