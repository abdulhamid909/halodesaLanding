<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ArticlesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title'  => 'required',
            'slug'    => 'required',
            'content'  => 'required',
            'tag'  => '',
            'category_id'  => 'required',
            'status'  => 'required',
            'meta_title'  => 'max:70',
            'meta_keyword'  => '',
            'meta_description'  => 'max:160',
            'image'     => 'max:2000|mimes:jpeg,gif,png'
        ];

        $lastUrl = \GLobalHelper::lastUrl();

        if(is_numeric($lastUrl)) :
        else :
        endif;

        return $rules;
    }
}
