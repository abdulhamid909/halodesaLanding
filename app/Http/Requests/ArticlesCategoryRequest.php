<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ArticlesCategoryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'slug' => 'required',
            'description' => 'required',
        ];
        $last    = \GLobalHelper::lastUrl();  


        if(is_numeric($last)) : 
            $rules['category_name'] = 'required|unique:category_article,category_name,'.$last.',id';
        else :
            $rules['category_name'] = 'required';
        endif;

        return $rules;
    }
}
