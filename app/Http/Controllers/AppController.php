<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class AppController extends Controller
{
    
    protected $model;
    protected $title = "Halo Desa";
    protected $url = "/";
    protected $folder = "app";
    protected $form;

    public function getIndex(){
        $data['title'] = "Selamat Datang";
        return view($this->folder.'.index', $data);
    }

    public function getFitur(){
        $data['title'] = "Fitur";
        return view($this->folder.'.features', $data);
    }

    public function getKontak(){
        $data['title'] = "Kontak";
        return view($this->folder.'.contact', $data);
    }

    public function getPricingDetail($parameter=""){

        if ($parameter=="") return redirect($this->url);

        $data['title'] = "Detail Paket ";
        $data['type']  = $parameter;
        return view($this->folder.'.pricing-detail', $data);
    }

    public function postSend(Request $request){

    }

}
