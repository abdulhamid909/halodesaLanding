<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\DataTables\ArticlesDatatables;
use App\Models as Md;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Forms\ArticlesForm;
use App\Http\Requests\ArticlesRequest;
use Illuminate\Support\Facades\Input;

class ArticlesController extends Controller
{
    protected $model;
    protected $title = "Data Artikel";
    protected $url = "articles";
    protected $folder = "module.articles";
    protected $form;

    public function __construct(
        Md\Articles $model
    )
    {
        $this->model    = $model;
        $this->form     = ArticlesForm::class;
    }

    public function getIndex()
    {
        $data['title'] = $this->title;
        $data['breadcrumb'] = $this->url;
        return view($this->folder.'.index', $data);
    }

    public function getCreate(FormBuilder $formBuilder)
    {
        $form = $formBuilder->create($this->form, [
            'method' => 'POST',
            'url' => $this->url.'/store'
        ]);

        return view($this->folder.'.form', [
            'title' => $this->title, 
            'form' => $form,
            'breadcrumb' => 'new-'.$this->url]);
    }

    public function postStore(ArticlesRequest $request=null, $id="")
    {
        $input = $request->except('save_continue');
        $result = '';

        if( \Input::hasFile('image'))
            $photo  = (new \ImageUpload($input))->upload();

        if($id == "" ) :

            $input['slug'] = str_slug($input['slug'],"-");
            $input['image'] = isset($photo) ? $photo : "" ;
            $input['created_by']    = "System";
            
            $query = $this->model->create($input);
            $result = $query->id;

        else :

            $input['slug'] = str_slug($input['slug'],"-");
            if(\Input::hasFile('image'))
                $input['image'] = isset($photo) ? $photo : "";

            $this->model->find($id)->update($input);
            $result = $id;

        endif;

        $save_continue = \Input::get('save_continue');
        $redirect = empty($save_continue)?$this->url:$this->url.'/edit/'.$result;

        return redirect($redirect)->with('message','Berhasil tambah data Artikel!');
    }

    public function getEdit(FormBuilder $formBuilder=null, $id="")
    {
        if ($id=="" || is_null($formBuilder)) return redirect($this->url);

        $edit = $this->model->find($id);

        $form = $formBuilder->create($this->form,[
            'method' => 'POST',
            'url' => $this->url.'/store/'.$id,
            'model' => $edit
        ]);

        return view($this->folder.'.form', ['title' => $this->title,
                                            'form' => $form,
                                            'breadcrumb' => 'edit-'.$this->url]);
    }

    public  function  getDelete($id ="")
    {
        if($id=="") return redirect($this->url);

        $groups = $this->model->find($id);

        $groups->delete();

        return redirect($this->url)->with('message','Berhasil hapus data Artikel!');

    }

    public function anyData(Request $request){
        return (new ArticlesDatatables($this->model))->make();
    }
}
