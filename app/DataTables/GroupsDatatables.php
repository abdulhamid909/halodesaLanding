<?php

namespace App\DataTables;

use App\User;
use Yajra\Datatables\Services\DataTable;

class GroupsDatatables 
{
    protected $model;
    public $data;

    public function __construct($model)
    {
        $this->model = $model;
        $this->getData();
    }

    protected function getData()
    {
        $this->data = $this
            ->model
            ->select('id','group_name','description','created_by','created_at')
            ->orderBy('id','asc');

    }

    public function make()
    {
        return \Datatables::of($this->data)
            ->addColumn('action','
                <div class="btn-group">
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action <span class="caret"></span></button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{!! url(GLobalHelper::indexUrl().\'/edit/\'.$id) !!}">Edit</a></li>
                        <li><a href="{!! url(GLobalHelper::indexUrl().\'/delete/\'.$id) !!}">Delete</a></li>
                    </ul>
                </div>
                ')
            ->editColumn('created_at','{!! GLobalHelper::formatDate($created_at) !!}')
            ->removeColumn('id')
            ->make();
    }

}
