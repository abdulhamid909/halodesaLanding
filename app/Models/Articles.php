<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Articles extends Model {

    protected $table = 'articles';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
        
	public function scopeTakeData($query){
		return $query->with(['category'])
					 ->select('id','category_id','title','content','status','show','updated_by','created_at')
					 ->orderBy('id', 'desc');
	}

    public function category() {
        return $this->hasOne('App\Models\ArticlesCategory', 'id' ,'category_id');
    }

}
?>

