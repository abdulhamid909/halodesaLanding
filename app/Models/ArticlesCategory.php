<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArticlesCategory extends Model {

    protected $table = 'category_article';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
        
	public function scopeTakeData(){
		return self::select('id','category_name','description','created_by',
							'created_at')->orderBy('id', 'desc');
	}

}
?>

