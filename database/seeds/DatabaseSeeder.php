<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	/* Default Data */
        $connection = \Config::get('database.default');
        \DB::table('groups')->delete();
        \DB::table('groups')->insert(array([
                'id'          => 0,
                'group_name'  => 'Group BackEnd',
                'description' => 'Level Developer',
                'created_by'  => 'system',
                'created_at'  => \Carbon\Carbon::now(), 
                'updated_at'  => \Carbon\Carbon::now()
        ]));


        if($connection <> "mysql") 
            \DB::statement("SELECT pg_catalog.setval(pg_get_serial_sequence('groups', 'id'), ". "MAX(id)) FROM groups");

        $groups = \DB::table('groups')->first();
        \DB::table('users')->delete();
        \DB::table('users')->insert([
                'id'            => 1,
                'group_id'      => $groups->id,
                'name'          => 'Cah BackEnd',
                'username'      => 'cahbackend',
                'email'         => 'backend@spam4.me',
                'password'      => bcrypt('12345'),
                'status'        => 1,
                'created_by'    => 'system',
                'created_at'    => \Carbon\Carbon::now(), 
                'updated_at'    => \Carbon\Carbon::now()
        ]);
        
        if($connection <> "mysql") 
            \DB::statement("SELECT pg_catalog.setval(pg_get_serial_sequence('users', 'id'), ". "MAX(id)) FROM users");
        
        $users = \DB::table('users')->first();

        \DB::table('user_detail')->insert([
                'id'             => 1,
                'address'        => 'Address',
                'image'          => '.',
                'user_id'        => $users->id,
                'created_at'    => \Carbon\Carbon::now(), 
                'updated_at'    => \Carbon\Carbon::now()
        ]);
        
        if($connection <> "mysql") 
            \DB::statement("SELECT pg_catalog.setval(pg_get_serial_sequence('user_detail', 'id'), ". "MAX(id)) FROM user_detail");


        // $this->call(UsersTableSeeder::class);
    }
}
