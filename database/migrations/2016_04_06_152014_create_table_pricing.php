<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePricing extends Migration
{
    protected $table = "pricing";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if (!Schema::hasTable($this->table)) {

          Schema::create($this->table, function (Blueprint $table) {

              $table->engine = 'InnoDB';
              /** Primary key  */
              $table->increments('id');

              /** Main data  */
              $table->string('title');
              $table->string('pay_method');
              $table->integer('price');
              $table->enum('type', ['basic','standar','advanced','unlimited'])->default('basic');

              $table->text('description')->nullable();

              /* Action */
              $table->string('created_by')->default('system');
              $table->string('updated_by')->default('system');
              $table->nullableTimestamps();


          });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
