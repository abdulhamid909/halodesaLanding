<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableArticles extends Migration
{
    protected $table = "articles";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if (!Schema::hasTable($this->table)) {

          Schema::create($this->table, function (Blueprint $table) {

              $table->engine = 'InnoDB';
              /** Primary key  */
              $table->increments('id');
              $table->integer('category_id')->unsigned();

              /** Main data  */
              $table->string('title');
              $table->string('slug');
              $table->text('content');
              $table->string('image')->nullable();
              $table->string('tag')->nullable();
              $table->tinyInteger('status')->default(0);

              /* Meta Data */
              $table->string('meta_title')->nullable();
              $table->string('meta_keyword')->nullable();
              $table->string('meta_description')->nullable();

              /* Action */
              $table->string('created_by')->default('system');
              $table->string('updated_by')->default('system');
              $table->tinyInteger('show')->default(0);
              $table->nullableTimestamps();

              $table->foreign('category_id')->references('id')
                ->on('category_article');

          });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
